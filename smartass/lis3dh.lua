
--[[

LIS3DH SPI accelerometer interface

Copyright 2014 Smart Avionics Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

--]]

local bit = bit
local band = bit.band
local lshift = bit.lshift
local rshift = bit.rshift

local spi = spi
local spi_write = spi.write
local spi_readwrite = spi.readwrite

local pio = pio
local setpin = pio.pin.setval

local LIS3DH_SPI = 0
local LIS3DH_SPI_CLOCK = 1000000

local LIS3DH_CS_PIN = pio.PB_12

local SCALING = 1000 / 8192

local function setup_spi ()
  spi.setup(LIS3DH_SPI, spi.MASTER, LIS3DH_SPI_CLOCK, 1, 1, 8)
end

local function select ()
  setpin(0, LIS3DH_CS_PIN)
end

local function deselect ()
  setpin(1, LIS3DH_CS_PIN)
end

local function reset ()
  setup_spi()
  select()
  local result = spi_readwrite(LIS3DH_SPI, 0x8f, 0xff)
  deselect()
  if result[2] ~= 0x33 then
    error(string.format("lis3dh.reset() gets WHO_AM_I = 0x%x (should be 0x33)", result[2]))
  end
  --[[
  select()
  spi_write(LIS3DH_SPI, 0x1f, 0xc0) -- TEMP_CFG_REG (ADC_PD = 1, TEMP_EN = 1)
  deselect()
  --]]
  select()
  spi_write(LIS3DH_SPI, 0x20, 0x37) -- CTRL_REG1 (25Hz output, normal mode, all axes enabled)
  deselect()
  select()
  spi_write(LIS3DH_SPI, 0x23, 0x98) -- CTRL_REG4 (BDU = 1, +/- 4G, HR = 1)
  deselect()
end

local function read()
  local result
  --[[
  select()
  result = spi_readwrite(LIS3DH_SPI, 0x87, 0xff)
  deselect()
  print(string.format("STATUS_REG_AUX = 0x%x", result[2]))
  select()
  result = spi_readwrite(LIS3DH_SPI, 0xcc, 0xff, 0xff)
  deselect()
  print(string.format("ADC3 = %d, %d", result[2], result[3]))
  select()
  result = spi_readwrite(LIS3DH_SPI, 0xa7, 0xff)
  deselect()
  print(string.format("STATUS_REG = 0x%x", result[2]))
  --]]
  select()
  result = spi_readwrite(LIS3DH_SPI, 0xe8, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff)
  deselect()
  --print(string.format("%d, %d, %d, %d, %d, %d", result[2], result[3], result[4], result[5], result[6], result[7]))
  local x = (result[2] + result[3] * 256)
  if x >= 0x8000 then
    x = x - 0x10000
  end
  local y = (result[4] + result[5] * 256)
  if y >= 0x8000 then
    y = y - 0x10000
  end
  local z = (result[6] + result[7] * 256)
  if z >= 0x8000 then
    z = z - 0x10000 
  end
  --print(string.format("%d, %d, %d", x, y, z))
  return { x * SCALING, y * SCALING, z * SCALING }
end

local function init(void)
  pio.pin.setdir(pio.OUTPUT, LIS3DH_CS_PIN)
  deselect()
  reset()
end

return {
  init = init,
  read = read,
}

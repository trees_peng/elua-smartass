
--[[

autorun.lua - SmartASS Mk3 toplevel script

Copyright 2014 Smart Avionics Ltd.

--]]

local setpin = pio.pin.setval

require "smartass"

-- write error message to file on SD card

local function log_error(msg)
  local f = io.open("/mmc/error.txt", "w+")
  if f then
    f:write(msg .. "\n")
    f:close()
  end
end

-- example callback function that turns on an output when speed > 10

local function callback(speed, g)
  --[[
  if speed > 10 then
    setpin(1, OUT_1_PIN)
  else
    setpin(0, OUT_1_PIN)
  end
  --]]
end

-- call smartass function passing it our callback

local ok, msg = pcall(smartass, callback)
if not ok then
  print(msg)
  log_error(msg)
end


--[[

AD7798 SPI ADC interface

Copyright 2014 Smart Avionics Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

--]]

local bit = bit
local band = bit.band
local lshift = bit.lshift
local rshift = bit.rshift
local floor = math.floor

local spi = spi
local spi_write = spi.write
local spi_readwrite = spi.readwrite

local AD7798_SPI = 1
local AD7798_SPI_CLOCK = 1000000

local AD7798_MODE_CONT_CONV    = 0x00
local AD7798_MODE_SINGLE_CONV  = 0x20
local AD7798_MODE_IDLE         = 0x40
local AD7798_MODE_POWER_DOWN   = 0x60
local AD7798_MODE_INT_ZERO_CAL = 0x80
local AD7798_MODE_INT_FULL_CAL = 0xa0
local AD7798_MODE_SYS_ZERO_CAL = 0xc0
local AD7798_MODE_SYS_FULL_CAL = 0xd0

local AD7798_UPDATE_RATE_16_7 = 0xa
local AD7798_UPDATE_RATE_33_3 = 0x7
local AD7798_UPDATE_RATE_62   = 0x4
local AD7798_UPDATE_RATE_123  = 0x3

local AD7798_CONFIG_READ_AVDD     = 0x1007 -- unipolar, gain = 1, BUF disabled, chan 7 (AVdd monitor)
local AD7798_CONFIG_READ_PRESSURE = 0x0710 -- bipolar, gain = 128, BUF enabled, chan 0
local AD7798_CONFIG_READ_TEMP     = 0x1001 -- unipolar, gain = 1, BUF disabled, chan 2

local pio = pio
local setpin = pio.pin.setval

local AD7798_CS_PIN = pio.PB_1

local delay = tmr.delay

local function setup_spi ()
  spi.setup(AD7798_SPI, spi.MASTER, AD7798_SPI_CLOCK, 1, 1, 8)
end

local function select ()
  setpin(0, AD7798_CS_PIN)
end

local function deselect ()
  setpin(1, AD7798_CS_PIN)
end

local function reset ()
  setup_spi()
  select()
  -- send 32 bits of 1
  spi_write(AD7798_SPI, 0xff, 0xff, 0xff, 0xff)
  deselect()
  delay(tmr.SYS_TIMER, 1000) -- requires at least 500uS delay
end

local function read()
  select()
  -- write to comm reg - read from status reg
  while band(spi_readwrite(AD7798_SPI, 0x40, 0)[2], 0x80) ~= 0 do
    -- relax
  end
  -- write to comm reg - read from data reg
  local result = spi_readwrite(AD7798_SPI, 0x58, 0, 0)
  deselect()
  return lshift(result[2], 8) + result[3]
end

local last_config

local function config(config, rate)
  if config ~= last_config then
    --setup_spi()
    select()
    spi_write(AD7798_SPI, 0x10, rshift(config, 8), config, 0x08, AD7798_MODE_CONT_CONV, rate)
    deselect()
    last_config = config
    -- discard first sample to ensure we get the correct channel
    read()
  end
end

-- returns Volts
local function read_AVdd()
  config(AD7798_CONFIG_READ_AVDD, AD7798_UPDATE_RATE_123)
  return floor(read() * 6 * 1170 / 65535 + 0.5) / 1000 -- round to 3 decimal places
end

-- returns ADC counts
local function read_pressure()
  config(AD7798_CONFIG_READ_PRESSURE, AD7798_UPDATE_RATE_62)
  return read()
end

-- returns ADC counts
local function read_temperature()
  config(AD7798_CONFIG_READ_TEMP, AD7798_UPDATE_RATE_123)
  return read()
end

local function init(void)
  pio.pin.setdir(pio.OUTPUT, AD7798_CS_PIN)
  reset()
end

return {
  init = init,
  read = read,
  read_AVdd = read_AVdd,
  read_pressure = read_pressure,
  read_temperature = read_temperature,
}

-- SmartASS build configuration

return {
  cpu = 'stm32f405rg',
  components = {
    --sercon = { uart = 0, speed = 115200 },
    sercon = { uart = "cdc", speed = 115200 },
    romfs = true,
    cdc = { buf_size = 512 },
    advanced_shell = true,
    term = { lines = 25, cols = 80 },
    linenoise = { shell_lines = 10, lua_lines = 50 },
    stm32f4_enc = true,
    rpc = { uart = 0, speed = 115200 },
    adc = { buf_size = 2 },
    xmodem = true,
    cints = true,
    luaints = true,
    mmcfs = { cs_port = 0, cs_pin = 15, spi = 2 }
  },
  config = {
    egc = { mode = "alloc" },
    vtmr = { num = 1, freq = 20 },
    ram = { internal_rams = 2 },
    clocks = { external = 8000000, cpu = 168000000 }
  },
  modules = {
    generic = { 'all', "-i2c", "-net", "-rpc" },
    platform = 'all',
  },
  macros = { 'ELUA_BOARD_STM32F4_CAN_PIN_CONFIG_2' }
}


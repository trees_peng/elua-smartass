
BOARD = smartass

ROMFS_FILES = $(addprefix romfs/,$(notdir $(patsubst %.lua,%.lc,$(wildcard $(BOARD)/*.lua))))
#ROMFS_FILES = $(addprefix romfs/,$(notdir $(wildcard $(BOARD)/*.lua)))

#TARGET = lualong
#LUAC_CCN = int 32
TARGET = lua
LUAC_CCN = float 64

LUAC_CROSS = ./luac.cross

BIN = elua_$(TARGET)_$(BOARD).bin
DFU = elua_$(TARGET)_$(BOARD).dfu
HEX = elua_$(TARGET)_$(BOARD).hex

.PHONY: $(HEX)

$(HEX) $(BIN): $(ROMFS_FILES)
	./build_elua.lua board=$(BOARD) target=$(TARGET) prog

flash-swd: $(HEX)
	-openocd -f openocd.cfg -c "mb_flash $(HEX)"

erase:
	-openocd -f openocd.cfg -c "eraser"

semihost:
	-openocd -f openocd.cfg -c "arm semihosting enable;reset run"

flash-dfu: $(DFU)
	dfu-util -a 0 -s 0x08000000:leave -D $(DFU)

romfs/%.lc : $(BOARD)/%.lua Makefile $(LUAC_CROSS)
	$(LUAC_CROSS) -ccn $(LUAC_CCN) -cce little -o $@ $<

romfs/%.lua : $(BOARD)/%.lua $(LUAC_CROSS)
	$(LUAC_CROSS) -p $<
	cp $< $@

%.dfu : %.bin
	cp $< $@
	dfu-suffix -a $@

$(LUAC_CROSS) :
	lua cross-lua.lua
